/* ini-reader.hpp - Header ini reader class */
#ifndef INI_READER_HPP
#define INI_READER_HPP

#include <assert.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <stdexcept>

class IniReader {
public:
	IniReader(const std::string &);
	/* Checks if a key is present in the global section map */
	bool isSet(const std::string &) const;
	/* Checks if a key is present in the given section or in the global section */
	bool isSet(const std::string &, const std::string &) const;
	/* Returns the value if the key is present in the global section. */
	std::string getValue(const std::string &) const;
	/* Returns the value if the key is present in the specified section or the global section. */
	std::string getValue(const std::string &, const std::string &) const;
	std::string toString() const;
private:
	/* Reads the file based on the path set in the constructor */
	void readFile();
	void warningMessage(int, const std::string &);
	/* Returns true if key exists in the section, false otherwise */
	bool isSectionSet(const std::string &, const std::string &) const;
	void removeWhiteSpaceAtStart(std::string &);
	void removeWhiteSpaceAtEnd(std::string &);

	/* Map :: sectionName (String) -> ( Map :: Key (String) -> Value (String) ) */
	std::map<std::string, std::map<std::string, std::string>> iniMap;
	/* Map for global values */
	std::map<std::string, std::string> globalMap;
	std::string filePath;
	bool parsed = false;
};

#endif /* INI_READER_HPP */