#include <IniReader.hpp>

IniReader::IniReader(const std::string &path) {
	this->filePath = path;
	this->readFile();
}

void IniReader::warningMessage(int line, const std::string &message) {
	if(!this->parsed)
		throw std::runtime_error("[IniReader] Data was not parsed yet.");
	std::cerr<<"[IniReader] Warning: Line " << line << " of " << this->filePath << " " << message << "\n";
}

void IniReader::readFile() {
	if(this->parsed)
		throw std::runtime_error("[IniReader] File was already parsed!");

	std::ifstream file(this->filePath);
	std::string line;
	std::size_t found;
	int i = 0;
	int globalScope = true;
	std::string currentSection;

	if(file.fail())
		throw std::runtime_error("[IniReader] Error opening the file!");

	while(std::getline(file, line)) {
		std::string str1, str2;
		const char *pStr = line.c_str();

		++i;
		/* Empty line or commentary should not be parsed */
		if(pStr[0] == '#' || pStr[0] == ';' || line == "" || line.empty())
			continue;

		if(pStr[0] == '[') {
			globalScope = false;
			currentSection = std::move(line.substr(1, line.size() - 2));
			continue;
		}

		found = line.find("=");
		if(found == std::string::npos) {
			this->warningMessage(i, "has a malformed ini setting.");
			str1 = std::move(line);
			str2 = "";
		}
		else {
			str1 = line.substr(0, found);
			str2 = line.substr(found + 1);
			/* Also, remove the comment from the same line */
			found = str2.find("#");
			if(found != std::string::npos)
				str2 = str2.substr(0, found);
		}

		this->removeWhiteSpaceAtEnd(str1);
		this->removeWhiteSpaceAtStart(str2);

		/* If we're still in the global section, use the global section map */
		if(globalScope) {
			if(this->globalMap.count(str1))
				this->warningMessage(i, std::string("overrides existing key").append(str1));
			this->globalMap[str1] = str2;
		}
		/* Otherwise, use the regular setion map */
		else {
			try {
			if(this->iniMap.at(currentSection).count(str1))
				this->warningMessage(i, std::string("overrides existing key").append(str1));
			} catch (std::out_of_range &e) {

			}		
			this->iniMap[currentSection][str1] = str2;
		}


	}

	this->parsed = true;
}

bool IniReader::isSectionSet(const std::string &section, const std::string &key) const {
	if(!this->parsed)
		throw std::runtime_error("[IniReader] Data was not parsed yet.");

	if(!iniMap.count(section)){
		return false;
	}
	try {
		return iniMap.at(section).count(key);
	} 
	catch (const std::out_of_range &e) {
		return false;
	}

	return true;
}

bool IniReader::isSet(const std::string &section, const std::string &key) const {
	if(!this->parsed)
		throw std::runtime_error("[IniReader] Data was not parsed yet.");
	/* If the section or the key inside the section doesn't exist, check the global scope section */
	if(!this->isSectionSet(section, key))
		return this->isSet(key);

	return true;
}

bool IniReader::isSet(const std::string &key) const {
	if(!this->parsed)
		throw std::runtime_error("[IniReader] Data was not parsed yet.");
	return this->globalMap.find(key) != this->globalMap.end();
}

std::string IniReader::getValue(const std::string &key) const {
	if(!isSet(key))
		throw std::runtime_error("Key " + key + " not found");
	return this->globalMap.at(key);
}

std::string IniReader::getValue(const std::string &section, const std::string &key)
	const {
	if(!isSectionSet(section, key))
		return this->getValue(key);
	else
		return this->iniMap.at(section).at(key);
}

std::string IniReader::toString() const {
	std::string str;

	str = "IniReader -- Values:\n";
	/* Global scope */
	str.append("[Global]\n");
	for(auto &kv : this->globalMap)
		str.append(" -").append(kv.first).append(" => ").append(kv.second).append("\n");

	for(auto &kvSection : this->iniMap) {
		str.append(std::string("[").append(kvSection.first).append("]\n"));
		for(auto &kv : this->iniMap.at(kvSection.first))
			str.append(" -").append(kv.first).append(" => ").append(kv.second).append("\n");
	}
	str.append("---");

	return str;
}

void IniReader::removeWhiteSpaceAtStart(std::string &word) {
	auto p = word.c_str();
	/* control characters or spcace */
	while(*p != '\0' && (iscntrl(*p) || *p == 0x20))
		++p;
	if((unsigned int)(p - word.c_str()) < word.length())
		word = word.substr(p - word.c_str(), word.length());
}

void IniReader::removeWhiteSpaceAtEnd(std::string &word) {
	auto len = word.length();
    auto p = word.c_str() + len - 1;

	while(len && (iscntrl(*p) || *p == 0x20)) {
		--len;
		--p;
    }
    
	if(len > 0)
		word = word.substr(0, len);
}